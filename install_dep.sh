#!/bin/bash

# Función para verificar si un comando está disponible
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Instalación en Debian/Ubuntu
install_on_debian() {
  echo "Instalando en Debian/Ubuntu..."

  # Instalar Neovim
  if ! command_exists nvim; then
    echo "Instalando Neovim..."
    sudo apt update
    sudo apt install neovim
  else
    echo "Neovim ya está instalado."
  fi

  # Instalar ripgrep (opcional)
  if ! command_exists rg; then
    echo "Instalando ripgrep..."
    sudo apt install ripgrep
  else
    echo "ripgrep ya está instalado."
  fi

  # Instalar lazygit (opcional)
  if ! command_exists lazygit; then
    echo "Instalando lazygit..."
    sudo apt install lazygit
  else
    echo "lazygit ya está instalado."
  fi

  # Instalar bottom (opcional)
  if ! command_exists bottom; then
    echo "Instalando bottom..."
    sudo apt install bottom
  else
    echo "bottom ya está instalado."
  fi

  # Instalación de otras dependencias opcionales (Python, Node, etc.) no incluidas por simplicidad
}

# Instalación en Arch Linux
install_on_arch() {
  echo "Instalando en Arch Linux..."

  # Instalar Neovim
  if ! command_exists nvim; then
    echo "Instalando Neovim..."
    sudo pacman -Syu neovim
  else
    echo "Neovim ya está instalado."
  fi

  # Instalar ripgrep (opcional)
  if ! command_exists rg; then
    echo "Instalando ripgrep..."
    sudo pacman -Syu ripgrep
  else
    echo "ripgrep ya está instalado."
  fi

  # Instalar lazygit (opcional)
  if ! command_exists lazygit; then
    echo "Instalando lazygit..."
    sudo pacman -Syu lazygit
  else
    echo "lazygit ya está instalado."
  fi

  # Instalar bottom (opcional)
  if ! command_exists bottom; then
    echo "Instalando bottom..."
    sudo pacman -Syu bottom
  else
    echo "bottom ya está instalado."
  fi

  # Instalación de otras dependencias opcionales (Python, Node, etc.) no incluidas por simplicidad
}

# Instalación en macOS (Homebrew)
install_on_macos() {
  echo "Instalando en macOS..."

  # Instalar Homebrew si no está instalado
  if ! command_exists brew; then
    echo "Instalando Homebrew..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  else
    echo "Homebrew ya está instalado."
  fi

  # Instalar Neovim
  if ! command_exists nvim; then
    echo "Instalando Neovim..."
    brew install neovim
  else
    echo "Neovim ya está instalado."
  fi

  # Instalar ripgrep (opcional)
  if ! command_exists rg; then
    echo "Instalando ripgrep..."
    brew install ripgrep
  else
    echo "ripgrep ya está instalado."
  fi

  # Instalar lazygit (opcional)
  if ! command_exists lazygit; then
    echo "Instalando lazygit..."
    brew install lazygit
  else
    echo "lazygit ya está instalado."
  fi

  # Instalar bottom (opcional)
  if ! command_exists bottom; then
    echo "Instalando bottom..."
    brew install bottom
  else
    echo "bottom ya está instalado."
  fi

  # Instalación de otras dependencias opcionales (Python, Node, etc.) no incluidas por simplicidad
}

# Función principal para detectar la distribución y llamar a la instalación correspondiente
install_dependencies() {
  if [[ "$(uname)" == "Linux" ]]; then
    if [[ -x "$(command -v apt)" ]]; then
      install_on_debian
    elif [[ -x "$(command -v pacman)" ]]; then
      install_on_arch
    else
      echo "Distribución Linux no soportada."
      exit 1
    fi
  elif [[ "$(uname)" == "Darwin" ]]; then
    install_on_macos
  else
    echo "Sistema operativo no soportado."
    exit 1
  fi
}

# Llamada a la función principal de instalación
install_dependencies

echo "Instalación completa."
