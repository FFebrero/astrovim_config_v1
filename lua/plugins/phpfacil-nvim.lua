--- @type LazySpec
return {
  "javierfebrero/phpfacil-nvim",
  -- dir = "~/Proxectos/phpfacil-nvim",
  -- event = "LspAttach",
  lazy = true,
  cmd = {
    "PhpFacilFirma",
    "PhpFacilConstant",
    "PhpFacilController",
    "PhpFacilInterface",
    "PhpFacilAbstractClass",
    "PhpFacilEnum",
    "PhpFacilTrait",
    "PhpFacilComentario",
  },
}
