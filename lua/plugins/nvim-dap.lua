return {
  "mfussenegger/nvim-dap",
  config = function()
    local dap = require "dap"
    -- python --
    dap.adapters.python = {
      type = "executable",
      command = "path/to/virtualenvs/debugpy/bin/python",
      args = { "-m", "debugpy.adapter" },
    }

    dap.configurations.python = {
      {
        -- The first three options are required by nvim-dap
        type = "python", -- the type here established the link to the adapter definition: `dap.adapters.python`
        request = "launch",
        name = "Launch file",

        -- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options

        program = "${file}", -- This configuration will launch the current file if used.
        pythonPath = function()
          -- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
          -- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
          -- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
          local cwd = vim.fn.getcwd()
          if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
            return cwd .. "/venv/bin/python"
          elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
            return cwd .. "/.venv/bin/python"
          else
            return "/usr/bin/python"
          end
        end,
      },
    }
    dap.adapters.php = {
      type = "executable",
      command = "node",
      args = { "~/.paquetes/vscode-php-debug/out" },
    }

    -- Debug configurations specific to PHP
    dap.configurations.php = {
      {
        type = "php",
        request = "launch",
        name = "Laravel",
        port = 9003,
        pathMappings = {
          ["/var/www/app"] = "${workspaceFolder}",
        },
      },
      {
        type = "php",
        request = "launch",
        name = "DevilBox Joomla",
        port = 9003,
        pathMappings = {
          ["/shared/httpd/datos-tenerife/htdocs"] = "/home/javier/Proxectos/devilbox/data/www/datos-tenerife/htdocs",
        },
      },
      {
        type = "php",
        request = "launch",
        name = "Symfony",
        port = 9003,
        pathMappings = {
          ["/app"] = "${workspaceFolder}",
        },
      },
    }
  end,
}
