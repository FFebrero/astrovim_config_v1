--if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- AstroCommunity: import any community modules here
-- We import this file in `lazy_setup.lua` before the `plugins/` folder.
-- This guarantees that the specs are processed before any user plugins.

---@type LazySpec
return {
  "AstroNvim/astrocommunity",
  -- packs de linguaxes
  { import = "astrocommunity.pack.lua" },
  { import = "astrocommunity.pack.php" },
  { import = "astrocommunity.pack.html-css" },
  { import = "astrocommunity.pack.python" },
  { import = "astrocommunity.pack.json" },
  -- { import = "astrocommunity.pack.sql" },
  { import = "astrocommunity.pack.toml" },
  { import = "astrocommunity.pack.yaml" },
  { import = "astrocommunity.pack.bash" },
  { import = "astrocommunity.pack.typescript" },
  -- Para xestion de proyectos
  { import = "astrocommunity.project.projectmgr-nvim" },
  -- buscar/reemplazar
  { import = "astrocommunity.project.nvim-spectre" },
  -- IA AXUDA
  -- { import = "astrocommunity.completion.codeium-nvim" },
  -- NVCHAD configuraciones
  { import = "astrocommunity.recipes.telescope-nvchad-theme" },
  { import = "astrocommunity.recipes.heirline-nvchad-statusline" },
  { import = "astrocommunity.keybinding.nvcheatsheet-nvim" },
  -- TEMAS leader ft
  { import = "astrocommunity.colorscheme.tokyonight-nvim" },
  { import = "astrocommunity.colorscheme.cyberdream-nvim" },
  { import = "astrocommunity.colorscheme.catppuccin" },
  { import = "astrocommunity.colorscheme.citruszest-nvim" },
  { import = "astrocommunity.colorscheme.kanagawa-nvim" },
  { import = "astrocommunity.colorscheme.oxocarbon-nvim" },
  { import = "astrocommunity.colorscheme.dracula-nvim" },
  { import = "astrocommunity.colorscheme.rose-pine" },
  { import = "astrocommunity.colorscheme.monokai-pro-nvim" },

  --     astrocommunity/diagnostics/lsp_lines-nvim
  -- mostrar errores en arbol
  -- { import = "astrocommunity.diagnostics.lsp_lines-nvim" },
  -- Desactivar hits en modo insert
  -- { import = "astrocommunity.recipes.astrolsp-no-insert-inlay-hints" },
  -- multicursor ( contrl fechas)
  { import = "astrocommunity.editing-support.multiple-cursors-nvim" },
  -- mover lineas como se desea (alt hjkl)
  { import = "astrocommunity.editing-support.vim-move" },
  -- DOC linguaxes
  { import = "astrocommunity.editing-support.neogen" },
  -- CoC
  -- { import = "astrocommunity.lsp.coc-nvim" },
}
