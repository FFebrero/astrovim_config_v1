-- Import the 'dap' module
local dap = require "dap"

-- Configure the debugger adapter for PHP
dap.adapters.php = {
  type = "executable",
  command = "node",
  args = {
    require("mason-registry").get_package("php-debug-adapter"):get_install_path() .. "/extension/out/phpDebug.js",
  },
}

-- Debug configurations specific to PHP
dap.configurations.php = {
  {
    type = "php",
    request = "launch",
    name = "Devilbox PHP",
    port = 9000,
    pathMappings = {
      ["/shared/httpd/datos-pro/htdocs"] = "${workspaceFolder}/htdocs",
    },
  },
  {
    type = "php",
    request = "launch",
    name = "Laravel",
    port = 9003,
    pathMappings = {
      ["/var/www/app"] = "${workspaceFolder}",
    },
  },
  {
    type = "php",
    request = "launch",
    name = "Symfony",
    port = 9003,
    pathMappings = {
      ["/app"] = "${workspaceFolder}",
    },
  },
  {
    name = "Launch currently open script",
    type = "php",
    request = "launch",
    program = "${file}",
    port = 9003,
    cwd = "${workspaceFolder}",
    console = "integratedTerminal",
  },
}
